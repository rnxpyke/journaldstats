#!/bin/bash
awk 'BEGIN{a=1;s=""};
    {if ($1==s) a++;
    else {
        if(""!=s) {print s, a};
        s=$1;
        a=1}
    }'