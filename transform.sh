#!/bin/bash
awk 'BEGIN{split ("Mon Tue Wed Thu Fri Sat Sun", parts);
    for (i in parts) dict[parts[i]]=""};
    {if ($1 in dict) print $2, $3}'